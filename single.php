<?php get_header(); ?>

<?php get_template_part('incl/parts/hero-single'); ?>

<section class="s-single">
  <div class="s-single__container container">
    <div class="c-content">
      <?= get_the_content(); ?>
        <?php
        $url = '';
        if(get_field('post_old_url')):
            $url = get_field('post_old_url');
        else:
            $url = get_permalink();
        endif;

        ?>
        <div class="fb-like" data-href="<?= $url ?>" data-width="" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="false"></div>
        <div class="fb-share-button" data-href="<?= $url ?>" data-layout="button_count" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?= $url ?>&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Udostępnij</a></div>
    </div>
    <div class="s-single__tags">
      <?php
      $tags = get_the_tags();
      if($tags):
          foreach ($tags as $tag):?>
            <a href="#" class="o-button-grey">
              <?= $tag->name; ?>
            </a>
          <?php endforeach;
      endif; ?>
    </div>
  </div>
</section>

<div class="s-comments container">
  <h2 class="o-heading">Zostaw komentarz</h2>
  <?php comments_template(); ?>
</div>

<!-- OTHER POSTS -->

<section class="s-others">
  <div class="container">
    <h2 class="o-heading">Czytaj także</h2>
    <?php get_template_part('incl/parts/other-posts'); ?>
  </div>
</section>

<?php //get_template_part('incl/parts/ebook'); ?>

<?php get_footer(); ?>
