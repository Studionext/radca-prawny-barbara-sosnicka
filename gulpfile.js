const gulp = require('gulp'),
      cleanCSS = require('gulp-clean-css'),
      concat = require('gulp-concat'),
      babel = require('gulp-babel'),
      uglify = require('gulp-uglify-es').default,
      autoprefixer = require('gulp-autoprefixer');

gulp.task("css", () => {
  gulp.src('./assets/css/styles.css')
    .pipe(autoprefixer())
    .pipe(cleanCSS())
    .pipe(gulp.dest('./assets/css'))
});

// First jsBabel, then jsDist

gulp.task("jsBabel", () => {
  return gulp.src([
    "./src/js/_*.js"
  ])
    .pipe(concat('concat.js'))
    .pipe(babel({
        presets: ['@babel/env']
    }))
    .pipe(gulp.dest('./src/js'))
});

gulp.task("jsDist", () => {
  return gulp.src([
    "./assets/vendors/slick-master/slick/slick.js",
    "./assets/vendors/blazy-master/blazy.min.js",
    "./src/js/bLazyHandler.js",
    "./src/js/concat.js"
  ])
    .pipe(concat('dist.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./assets/js'))
});
