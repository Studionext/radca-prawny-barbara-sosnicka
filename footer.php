<!-- For IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.1/html5shiv.js"></script>
<![endif]-->
<?php wp_footer(); ?>
<footer class="footer">
  <div class="container footer__container">
    <div class="footer__time4">
      <img class="footer__logo--desktop" src="<?= TEMPLATE_URL; ?>assets/img/logos/logo_white_small.png" alt="Logo"/>
      <p class="footer__time__text">2019 &copy; made with passion by <a href="https://time4.pl/" target="_blank" rel="noopener noreferrer">time4</a></p>
    </div>
    <div class="footer__navigation">
      <button class="footer__navigation__hamburger rwd__menu">
        <span class="fas fa-bars rwd__menu active"></span>
        <span class="fas fa-times rwd__menu"></span>
      </button>
      <div class="footer__call">
        <img src="<?= TEMPLATE_URL; ?>assets/img/icons/phone_icon_small.png" alt="Phone icon"/>
        <a href="tel:+48504333785">Zadzwoń!</a></div>
        <?php wp_nav_menu( array( 'theme_location' => 'extra-menu', 'container_class' => 'extra__menu rwd__menu' ) ); ?>
    </div>
  </div>
</footer>

<aside class="c-cookie-bar" id="js-cookieBar">
    <div class="c-cookie-bar__inner">
        <?php the_field('settings_cookies', 'option') ?>
        <button class="c-cookie-bar__button">Rozumiem</button>
    </div>
</aside>


<!-- SCRIPTS -->

<!-- <script src='<?= TEMPLATE_URL; ?>assets/vendors/slick-master/slick/slick.min.js'></script>
<script src='<?= TEMPLATE_URL; ?>assets/vendors/blazy-master/blazy.min.js'></script>
<script src='<?= TEMPLATE_URL; ?>src/js/bLazyHandler.js'></script>
<script src='<?= TEMPLATE_URL; ?>src/js/_slickOthers.js'></script>
<script src='<?= TEMPLATE_URL; ?>src/js/_burger.js'></script>
<script src='<?= TEMPLATE_URL; ?>src/js/_smoothScroll.js'></script> -->

</body>
</html>
