<!DOCTYPE html>
<html class="no-js"
      lang="pl-PL">

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <title><?php wp_title(); ?></title>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible"
          content="ie=edge">
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;700&display=swap"
          rel="stylesheet">

    <link rel="stylesheet"
          href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
          crossorigin="anonymous">
    <!-- <link href="https://fonts.googleapis.com/css?family=Lora|Playfair+Display:400,700" rel="stylesheet"/> -->
    <!-- <link rel="shortcut icon" href="<?= TEMPLATE_URL; ?>favicon.ico"/> -->
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <div id="fb-root"></div>
    <script async
            defer
            crossorigin="anonymous"
            src="https://connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v5.0"></script>