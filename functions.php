<?php
include("incl/variables.php");
include("incl/general.php");
include("incl/performance.php");
include("incl/scripts.php");

function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Menu główne' ),
      'extra-menu' => __( 'Menu w stopce' )
    )
  );
}
add_action( 'init', 'register_my_menus' );

add_post_type_support( 'post', array(
    'excerpt',
    'comments'
  )
);

// Move Yoast to bottom
function yoasttobottom() {
    return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');


if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title' => 'Ustawienia ciasteczek',
        'menu_title' => 'Ustawienia ciasteczek',
        'menu_slug' => 'page-options',
        'capability' => 'edit_posts',
        'parent_slug' => '',
        'position' => 25,
        'icon_url' => 'dashicons-edit'
    ));
}
