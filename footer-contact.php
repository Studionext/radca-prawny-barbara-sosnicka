<!-- For IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.1/html5shiv.js"></script>
<![endif]-->
<?php wp_footer(); ?>
<footer class="footer">
  <div class="container footer__container">
    <div class="footer__time4 footer__contact__time4"><img class="footer__logo--desktop" src="<?= TEMPLATE_URL; ?>assets/img/logos/logo_white_small.png" alt="Logo"/>
      <p class="footer__time__text">2019 &copy; made with passion by <a href="https://time4.pl/" target="_blank" rel="noopener noreferrer">time4</a></p>
    </div>
    <div class="footer__navigation">
      <button class="footer__navigation__hamburger rwd__menu"><span class="fas fa-bars rwd__menu active"></span><span class="fas fa-times rwd__menu"></span></button>
      <div class="footer__call"><img src="<?= TEMPLATE_URL; ?>assets/img/icons/phone_icon_small.png" alt="Phone icon"/><a href="tel:+48504333785">Zadzwoń!</a></div>
      <ul class="footer__menu rwd__menu">
        <li class="footer__menu__item footer__logo__container"><img src="<?= TEMPLATE_URL; ?>assets/img/logos/logo_white_small.png" alt="Logo"/></li>
        <li class="footer__menu__item"><a class="footer_menu__link" href="#">Strona główna</a></li>
        <li class="footer__menu__item"><a class="footer_menu__link" href="#">Blog</a></li>
        <li class="footer__menu__item"><a class="footer_menu__link" href="#">O kancelarii</a></li>
        <li class="footer__menu__item"><a class="footer_menu__link" href="#">Kontakt</a></li>
        <li class="footer__menu__item footer__social__container"><a href="https://www.facebook.com/RadcaprawnyBarbaraSosnicka/" target="_blank" rel="noopener noreferrer"><span class="fab fa-facebook-f"></span></a><a href="https://www.facebook.com/RadcaprawnyBarbaraSosnicka/" target="_blank" rel="noopener noreferrer"><span class="fab fa-twitter"></span></a></li>
      </ul>
    </div>
  </div>
</footer>
