<?php get_header(); ?>

<?php get_template_part('incl/parts/hero-single'); ?>

<section class="s-single">
    <div class="s-single__container container">
        <div class="c-content">
            <h2 style="text-align: center">
                Strona o podanym adresie nie istnieje.
            </h2>
        </div>
    </div>
</section>

<!-- OTHER POSTS -->

<section class="s-others">
    <div class="container">
        <h2 class="o-heading">Ostatnio na blogu</h2>
        <?php get_template_part('incl/parts/other-posts'); ?>
    </div>
</section>

<?php get_template_part('incl/parts/ebook'); ?>

<?php get_footer(); ?>
