const gulp = require('gulp');
const pug = require('gulp-pug');
const stylus = require('gulp-stylus');
const plumber = require('gulp-plumber');
const postcss = require('gulp-postcss');
const csso = require('postcss-csso');
const autoprefixer = require('autoprefixer');
const gulpPugBeautify = require('gulp-pug-beautify');
const sourcemaps = require('gulp-sourcemaps');
const cache = require('gulp-cached');
const bs = require("browser-sync").create();
const beeper = require('beeper');
const signale = require('signale');
const changed = require('gulp-changed');
const prettify = require('gulp-prettify');
const remember = require('gulp-remember');

const NODE_ENV = process.env.NODE_ENV || 'development';
const isDev = NODE_ENV === 'development' || NODE_ENV === 'dev';
const isProd = NODE_ENV === 'production' || NODE_ENV === 'prod';

function handleError(err) {
    beeper();
    signale.error(err);
    this.emit('end');
}

const postcssOptions = isProd
    ? [
        autoprefixer,
        csso({
            restructure: true,
            comments: false
        })
    ]
    : [autoprefixer];

gulp.task('stylus', stylusFunction);
gulp.task('pug', pugFunction);
gulp.task('serve', gulp.series('stylus', serveFunction));

function serveFunction(done) {
    bs.init({
        open: false,
        server: {
            baseDir: "./build"
        }
    });
    gulp.watch(['**/*.styl', 'modules/**/*.styl'], gulp.series('stylus'));
    gulp.watch("modules/**/*.pug", gulp.series('pug'));

    gulp.watch("app/*.html").on('change', bs.reload);
    gulp.watch("app/js/**/*.js").on('change', bs.reload);
    gulp.watch("app/img/**/*").on('change', bs.reload);

    done();
}

function stylusFunction() {
    let isSuccess = true;
    return gulp.src("styles/main.styl")
        .pipe(sourcemaps.init())
        .pipe(stylus()).on('error', handleError)
        .pipe(sourcemaps.write())
        .pipe(gulp.dest("build/css"))
        .pipe(bs.stream())
        .on('error', handleError)
}

function pugFunction() {
    return gulp.src(['modules/*.pug','!modules/all-in.pug'])
        .pipe(plumber())
        .pipe(pug({
            pretty: true
        })).on('error', handleError)
        .pipe(gulp.dest('build/'))
        .pipe(bs.stream(true))
}

/* DELETED
    .pipe(changed('build/'))
    .pipe(prettify({indent_size: 4}))
*/

/*
gulp.task('pug', () => {
    return gulp
        .src(['modules/index.pug']) // Change for editing view
        .pipe(cache('pug'))
        .pipe(plumber())
        .pipe(pug({pretty: true}))
        //.pipe(gulpPugBeautify({omit_empty: true}))
        .pipe(gulp.dest('build'));
})

gulp.task('stylus', () => {
    return gulp
        .src('styles/main.styl')
        .pipe(cache('stylus'))
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(stylus())
        .pipe(sourcemaps.write())
        .pipe(postcss(postcssOptions))
        .pipe(gulp.dest('build/css'));
})

gulp.task('default', ['pug', 'stylus']);
//*/
