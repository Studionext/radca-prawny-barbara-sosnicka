<?php /* Template Name: Blog */ ?>

<?php get_header(); ?>

<?php get_template_part('incl/parts/hero-standard'); ?>

<section class="s-blog">
   <div class="s-blog__container container">
     <?php
       $query_posts = new WP_Query(array(
         'post_type' => 'post',
         'posts_per_page' => -1
       ));
       if ($query_posts->have_posts()): while ($query_posts->have_posts()): $query_posts->the_post();
       // var_dump(get_the_post_thumbnail());
     ?>
     <div class="s-blog__tile">
       <!-- <a class="c-tile-big" href="<?php the_permalink(); ?>"> -->
       <div class="c-tile-big">
         <div
           class="c-tile__photo c-tile-big__photo b-lazy"
           data-src="<?= wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium_large')[0]; ?>">
         </div>
         <div class="c-tile-big__tags">
           <?php
           $tags = get_the_tags();
           if($tags):
               foreach ($tags as $tag):?>
                 <a href="#" class="o-button-grey">
                   <?= $tag->name; ?>
                 </a>
               <?php endforeach;
           endif; ?>

         </div>
         <h3 class="c-tile__heading c-tile-big__heading"><?php the_title(); ?></h3>
         <div class="c-tile-big__excerpt"><?php the_excerpt(); ?></div>
         <div class="c-tile-big__extras">
           <a href="<?php the_permalink(); ?>" class="o-button">Czytaj więcej</a>
           <p class="c-date c-tile-big__date">
             <img src="<?= TEMPLATE_URL; ?>assets/img/icons/calendar.png" alt="Data">
             <?php the_time('j F Y, g:i'); ?>
           </p>
         </div>
       </div>
       <!-- </a> -->
     </div>
     <?php endwhile; endif; ?>
   </div>
</section>

<?php //get_template_part('incl/parts/ebook'); ?>

<?php get_footer(); ?>
