<?php /* Template Name: O kancelarii */ ?>

<?php get_header(); ?>

<?php get_template_part('incl/parts/hero-standard'); ?>

<section class="s-about">
   <div class="s-about__container container">
     <?php
       $tiles = get_field('about_tiles');
       foreach ($tiles as $tile):
      ?>
     <div class="s-about__tile">
       <h2 class="o-heading"><?= $tile['title']; ?></h2>
       <div class="s-about__content">
         <?= $tile['content']; ?>
       </div>
     </div>
    <?php endforeach; ?>
   </div>
</section>

<?php //get_template_part('incl/parts/ebook'); ?>

<?php get_footer(); ?>
