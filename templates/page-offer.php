<?php /* Template Name: Oferta */ ?>

<?php get_header(); ?>

<?php get_template_part('incl/parts/hero-standard'); ?>

<section class="s-offer">
  <?php
    $n = 0;
    $offers = get_field('offers');
    foreach ($offers as $offer):
        $n++;
   ?>
   <div id="offer-<?= $n ?>" class="s-offer__tile">
     <div class="s-offer__container container">
       <div class="s-offer__row row">
         <div class="s-offer__content col-12 col-lg-6">
           <?= $offer['content']; ?>
         </div>
         <div class="col-12 col-lg-6">
           <div
            class="s-offer__photo b-lazy"
            data-src="<?= $offer['photo']; ?>">
            </div>
         </div>
       </div>
     </div>
   </div>
   <?php endforeach; ?>
</section>

<?php get_footer(); ?>
