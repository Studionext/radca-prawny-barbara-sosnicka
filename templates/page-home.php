<?php /* Template Name: Kancelaria Homepage */ ?>

<?php get_header(); ?>

<?php get_template_part('incl/parts/homepage/hero', '1'); ?>
<?php get_template_part('incl/parts/homepage/about', '1'); ?>
<?php get_template_part('incl/parts/homepage/offer', '1'); ?>
<?php get_template_part('incl/parts/homepage/blog', '1'); ?>
<?php get_template_part('incl/parts/homepage/contact', '1'); ?>
<?php //get_template_part('incl/parts/ebook'); ?>

<?php get_footer(); ?>
