<!DOCTYPE html>
<html class="no-js"
      lang="pl-PL">

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <title><?php wp_title(); ?></title>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible"
          content="ie=edge">
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;700&display=swap"
          rel="stylesheet">

    <link rel="stylesheet"
          href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
          crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Lora|Playfair+Display:400,700"
          rel="stylesheet" />
    <link rel="shortcut icon"
          href="<?= TEMPLATE_URL; ?>favicon.ico" />
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <header class="hero__contact"><a class="hero__mail"
           href="mailto:kontakt@kancelariasosnicka.pl">Kontakt@kancelariasosnicka.pl</a>
        <div class="hero__border__container"></div><img class="hero__image--small hero__contact__img--small"
             src="<?= TEMPLATE_URL; ?>assets/img/pics/contact_hero_pic.png"
             alt="Image" /><img class="hero__image--large hero__image--large--contact"
             src="<?= TEMPLATE_URL; ?>assets/img/pics/contact_hero_pic_large.png"
             alt="Image" />
        <div class="container">
            <h1 class="hero__header__contact"><?php the_title(); ?></h1>
            <div class="navigation">
                <?php
            //  echo "<div class='mobile-menu-close-container'>";
            //  echo "<button class='mobile-menu-close rwd_menu'><span class='fas fa-times'></span></button>";
            //  echo "</div>";
            $args = array(
                'theme_location' => 'main_menu',
                'container' => false,
                'link_before' => '<span>',
                'link_after' => '<span>',
                'items_wrap' => '<ul class="menu">%3$s</ul>',
                'depth' => 0
            );
            echo wp_nav_menu($args);
            ?>
            </div>
        </div>
    </header>