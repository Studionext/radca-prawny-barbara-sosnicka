$(document).ready(function(){

  $('.s-others__container').on('afterChange', function(event, slick, currentSlide, nextSlide){
    setTimeout(() => {
      window.scrollBy(0,10);
      window.scrollBy(0,-10);
    }, 100)
  });

  if ($('.s-others__container').length > 0) {
    $('.s-others__container').slick({
      infinite: false,
      // initialSlide: 1,
      slidesToShow: $('.s-others__container .s-others__tile').length < 4 ? $('.s-check__latest .s-others__tile').length : 4,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 1500,
          settings: {
            initialSlide: 1,
            slidesToShow: $('.s-others__container .s-others__tile').length < 3 ? $('.s-check__latest .s-others__tile').length : 3,
          }
        },
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            centerMode: true,
            centerPadding: '50px'            
          }
        }
      ]
    });
  }


});
