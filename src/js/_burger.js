$(document).ready(() => {

  if (document.querySelector('.footer__navigation__hamburger')){
    document.querySelector('.footer__navigation__hamburger')
      .addEventListener('click', () => {
        document
          .querySelectorAll('.rwd__menu')
          .forEach(item => item.classList.toggle('active'));
      });
  }

})
