$(document).ready(() => {

  $('#about__slide').on('click', function() {
    $('body, html').animate(
      {
        scrollTop: $('#about__section').offset().top
      },
      1000
    );
  });

})
