"use strict";

$(document).ready(function () {
  if (document.querySelector('.footer__navigation__hamburger')) {
    document.querySelector('.footer__navigation__hamburger').addEventListener('click', function () {
      document.querySelectorAll('.rwd__menu').forEach(function (item) {
        return item.classList.toggle('active');
      });
    });
  }
});

var cookies = function () {
  // Function used to set a cookie.
  // Needed params are:
  // Name - name of the cookie
  // Val - value of the cookie
  function setCookie(name, val, days, path, domain, secure) {
    if (navigator.cookieEnabled) {
      var cookieName = encodeURIComponent(name);
      var cookieVal = encodeURIComponent(val);
      var cookieText = cookieName + "=" + cookieVal;

      if (typeof days === "number") {
        var data = new Date();
        data.setTime(data.getTime() + days * 24 * 60 * 60 * 1000);
        cookieText += "; expires=" + data.toGMTString();
      }

      if (path) {
        cookieText += "; path=" + path;
      }

      if (domain) {
        cookieText += "; domain=" + domain;
      }

      if (secure) {
        cookieText += "; secure";
      }

      document.cookie = cookieText;
    }
  } // Read cookie value by name


  function getCookie(name) {
    if (document.cookie != "") {
      var _cookies = document.cookie.split(/; */);

      for (var i = 0; i < _cookies.length; i++) {
        var cookieName = _cookies[i].split("=")[0];

        var cookieVal = _cookies[i].split("=")[1];

        if (cookieName === decodeURIComponent(name)) {
          return decodeURIComponent(cookieVal);
        }
      }
    }
  }

  return {
    setCookie: setCookie,
    getCookie: getCookie
  };
}();

$(document).ready(function () {
  var cookieBar = document.getElementById('js-cookieBar'),
      likesCookies = Boolean(cookies.getCookie('cookieConsent')) || false;
  setTimeout(function () {
    cookieBar.addEventListener('click', function () {
      // Cookie name: cookieConsent, value: true, expiry: 360 days, path: '/'
      cookies.setCookie('cookieConsent', 'true', 360, '/');
      this.classList.remove('is-visible');
      setTimeout(function () {
        cookieBar.style.display = 'none';
      }, 200);
    });

    if (!likesCookies) {
      cookieBar.style.display = 'block';
      setTimeout(function () {
        cookieBar.classList.add('is-visible');
      }, 200);
    }
  }, 1000);
});
$(document).ready(function () {
  $('.s-others__container').on('afterChange', function (event, slick, currentSlide, nextSlide) {
    setTimeout(function () {
      window.scrollBy(0, 10);
      window.scrollBy(0, -10);
    }, 100);
  });

  if ($('.s-others__container').length > 0) {
    $('.s-others__container').slick({
      infinite: false,
      // initialSlide: 1,
      slidesToShow: $('.s-others__container .s-others__tile').length < 4 ? $('.s-check__latest .s-others__tile').length : 4,
      slidesToScroll: 1,
      arrows: false,
      responsive: [{
        breakpoint: 1500,
        settings: {
          initialSlide: 1,
          slidesToShow: $('.s-others__container .s-others__tile').length < 3 ? $('.s-check__latest .s-others__tile').length : 3
        }
      }, {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2
        }
      }, {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          centerMode: true,
          centerPadding: '50px'
        }
      }]
    });
  }
});
$(document).ready(function () {
  $('#about__slide').on('click', function () {
    $('body, html').animate({
      scrollTop: $('#about__section').offset().top
    }, 1000);
  });
});