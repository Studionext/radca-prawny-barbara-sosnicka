$(document).ready(() => {

  var bLazy = new Blazy();

  $('.about__btn').on('click', function () {
    $('.about__text__container').toggleClass('show--full');
    if($('.about__text__container').hasClass('show--full')){
      $(this).text('Zwiń');
    } else{
      $(this).text('Rozwiń więcej')
    }
  });

})
