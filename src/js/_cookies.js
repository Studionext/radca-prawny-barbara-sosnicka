const cookies = (function() {

    // Function used to set a cookie.
    // Needed params are:
    // Name - name of the cookie
    // Val - value of the cookie
    function setCookie(name, val, days, path, domain, secure) {
        if (navigator.cookieEnabled) {

            const cookieName = encodeURIComponent(name);
            const cookieVal = encodeURIComponent(val);

            let cookieText = cookieName + "=" + cookieVal;

            if (typeof days === "number") {
                const data = new Date();
                data.setTime(data.getTime() + (days * 24*60*60*1000));
                cookieText += "; expires=" + data.toGMTString();
            }

            if (path) {
                cookieText += "; path=" + path;
            }

            if (domain) {
                cookieText += "; domain=" + domain;
            }

            if (secure) {
                cookieText += "; secure";
            }

            document.cookie = cookieText;
        }
    }


    // Read cookie value by name
    function getCookie(name) {
        if (document.cookie != "") {
            const cookies = document.cookie.split(/; */);

            for (let i=0; i<cookies.length; i++) {
                const cookieName = cookies[i].split("=")[0];
                const cookieVal = cookies[i].split("=")[1];

                if (cookieName === decodeURIComponent(name)) {
                    return decodeURIComponent(cookieVal);
                }
            }
        }
    }

    return {
        setCookie: setCookie,
        getCookie: getCookie
    }

})();

$(document).ready(() => {

    const cookieBar = document.getElementById('js-cookieBar'),
        likesCookies = Boolean(cookies.getCookie('cookieConsent')) || false;

    setTimeout(function () {
        cookieBar.addEventListener('click', function () {

            // Cookie name: cookieConsent, value: true, expiry: 360 days, path: '/'
            cookies.setCookie('cookieConsent', 'true', 360, '/');
            this.classList.remove('is-visible');
            setTimeout(function () {
                cookieBar.style.display = 'none';
            }, 200);

        });

        if (!likesCookies) {
            cookieBar.style.display = 'block';
            setTimeout(function () {
                cookieBar.classList.add('is-visible');
            }, 200);

        }
    }, 1000);

});