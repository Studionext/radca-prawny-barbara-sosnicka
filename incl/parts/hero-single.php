<header
  class="hero__contact s-hero s-hero-standard s-hero-single">
  <div class="s-hero-single__back" style="background-image: url('<?= get_the_post_thumbnail_url(); ?>')"></div>
  <div class="s-hero__frame">
    <div class="container">
      <div class="navigation">
      <?php
        the_post();
        $args = array(
            'theme_location' => 'main_menu',
            'container' => false,
            'link_before' => '<span>',
            'link_after' => '<span>',
            'items_wrap' => '<ul class="menu">%3$s</ul>',
            'depth' => 0
        );
        echo wp_nav_menu($args);
        ?>
      </div>
      <h1 class="hero__header__contact">
          <?php if(is_404()): ?>
          Błąd 404
          <?php else: ?>
          <?php the_title(); ?>
          <?php endif; ?>
      </h1>
      <p class="c-date s-hero__date">
        <?php if(is_single()): ?>
            <img src="<?= TEMPLATE_URL; ?>assets/img/icons/calendar.png" alt="Data">
            <?php the_time('j F Y, G:i'); ?>
        <?php endif; ?>
      </p>
<?php if(is_single()): ?>
    <div class="s-hero__author">
        <div class="s-hero__author-content">
            Autor:<br>
            <span><?= get_the_author_meta('first_name'); ?> <?= get_the_author_meta('last_name'); ?></span>
        </div>
        <div class="s-hero__author-photo" style="background-image: url('<?= get_avatar_url(get_the_author_meta('ID')); ?>')">
        </div>
    </div>
<?php endif; ?>
    </div>
    <a class="hero__mail" href="mailto:kontakt@kancelariasosnicka.pl">Kontakt@kancelariasosnicka.pl</a>
  </div>
</header>
