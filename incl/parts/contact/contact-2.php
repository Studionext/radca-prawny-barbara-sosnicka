<section class="contact contact__page">
  <div class="contact__border__container contact__page__border__container"><img class="contact__icon" src="<?= TEMPLATE_URL; ?>assets/img/icons/phone_icon.png" alt="Icon"/></div>
  <div class="container">
    <div class="contact__text__container contact__page__text__container">
      <div class="contact__page__text">
        <h2 class="contact__header">Porozmawiajmy</h2>
        <p class="contact__paragraph--1">osobiście, telefonicznie lub przez Skype’a.</p>
        <p class="contact__paragraph--2">Wspólnie znajdziemy najlepsze rozwiązanie.</p>
      </div>
      <address class="contact__page__address__container">
        <div class="mail__container"><span class="fas fa-envelope contact__page__icon"></span><a class="contact__page__mail" href="mailto:kontakt@kancelariasosnicka.pl">kontakt@kancelariasosnicka.pl</a></div>
        <div class="phone__container"><span class="fas fa-phone contact__page__icon"></span><a class="contact__page__phone" href="tel:+48504333785">+48 504 333 785</a></div>
        <div class="address__container"><span class="fas fa-map-marker-alt contact__page__icon"></span>
          <address class="contact__page__address">Tychy, Dąbrowskiego 87</address>
        </div>
      </address>
    </div>
  </div>
</section>
