<section class="offer">
  <div class="container offer__container">
    <h2 class="offer__header">sprawdź <br>ofertę</h2>
    <div class="offer__text__container">
    <ul class="offer__list">
        <li class="offer__list__item">
          <a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Oferta' ) ) ); ?>#offer-5">
            Ochrona Danych Osobowych RODO/GDPR
            <span class="o-caret o-caret--right b-lazy" data-src="<?= TEMPLATE_URL; ?>assets/img/icons/caret-right.png">
            </span>
          </a>
        </li>
        <li class="offer__list__item">
          <a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Oferta' ) ) ); ?>#offer-2">
            Prawo autorskie
            <span class="o-caret o-caret--right b-lazy" data-src="<?= TEMPLATE_URL; ?>assets/img/icons/caret-right.png">
            </span>
          </a>
        </li>
        <li class="offer__list__item">
          <a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Oferta' ) ) ); ?>#offer-3">
            E-commerce
            <span class="o-caret o-caret--right b-lazy" data-src="<?= TEMPLATE_URL; ?>assets/img/icons/caret-right.png">
            </span>
          </a>
        </li>
      </ul>
      <div class="offer__btn__container">
        <a target="_blank" href="<?php echo esc_url( get_permalink( get_page_by_title( 'Kontakt' ) ) ); ?>" class="offer__btn">Pokaż więcej</a>
      </div>
    </div>
  </div>
</section>
