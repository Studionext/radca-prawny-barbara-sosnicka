<header class="hero"><a class="hero__mail" href="mailto:kontakt@kancelariasosnicka.pl">Kontakt@kancelariasosnicka.pl</a>
  <div class="hero__border__container"></div>
    <img class="hero__image--small" src="<?= TEMPLATE_URL; ?>assets/img/pics/hero_img_small.png" alt="Image"/>
    <img class="hero__image--large" src="<?= TEMPLATE_URL; ?>assets/img/pics/hero_img_large.png" alt="Image"/>
  <button class="hero__btn--arrow" id="about__slide"><img src="<?= TEMPLATE_URL; ?>assets/img/icons/arrow_down.png" alt="Icon"/></button>
  <div class="container">
    <div class="hero__header--container">
      <h1 class="hero__header">Barbara Sośnicka</h1>
      <h2 class="hero__subheader">Kancelaria Radcy Prawnego</h2>
    </div>
    <div class="navigation">
    <?php
            //  echo "<div class='mobile-menu-close-container'>";
            //  echo "<button class='mobile-menu-close rwd_menu'><span class='fas fa-times'></span></button>";
            //  echo "</div>";
            $args = array(
                'theme_location' => 'main_menu',
                'container' => false,
                'link_before' => '<span>',
                'link_after' => '<span>',
                'items_wrap' => '<ul class="menu">%3$s</ul>',
                'depth' => 0
            );
            echo wp_nav_menu($args);
            ?>
    </div>
  </div>
</header>
