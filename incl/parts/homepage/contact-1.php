<section class="contact">
          <div class="contact__border__container"><img class="contact__icon" src="<?= TEMPLATE_URL; ?>assets/img/icons/phone_icon.png" alt="Icon"/></div>
          <div class="container">
            <div class="contact__text__container">
              <div class="contact__text">
                <h2 class="contact__header">Porozmawiajmy</h2>
                <p class="contact__paragraph--1">osobiście, telefonicznie lub przez Skype’a.</p>
                <p class="contact__paragraph--2">Wspólnie znajdziemy najlepsze rozwiązanie.</p>
              </div>
              <address class="contact__address__container"><a class="contact__mail contact__address__btn" href="mailto:kontakt@kancelariasosnicka.pl">kontakt@kancelariasosnicka.pl</a><a class="contact__phone contact__address__btn" href="tel:+48504333785">+48 504 333 785</a>
                <address class="contact__address contact__address__btn">Tychy, Dąbrowskiego 87</address>
              </address>
            </div>
          </div>
        </section>
