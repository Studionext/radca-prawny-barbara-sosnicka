<section class="about" id="about__section">
    <div class="container">
        <h2 class="about__header">Barbara Sośnicka - kim jestem?</h2>
        <div class="about__logo__container">
            <img class="about__logo" src="<?= TEMPLATE_URL; ?>assets/img/logos/logo_small.png" alt="Logo"/>
        </div>
        <div class="about__text__container">
            <p class="about__paragraph">Jestem radcą prawnym wpisanym na listę Okręgowej Izby Radców Prawnych w Opolu, specjalistką z
                zakresu prawa ochrony danych osobowych oraz praw autorskiego i konsumenckiego, a także
                inspektorem ochrony danych.
            </p>
            <p class="about__paragraph">
                Zajmuję się obsługą prawną przedsiębiorstw. Doradzam głównie podmiotom z branż: e-commerce,
                kreatywnej, HR i finansowej.
            </p>
            <p class="about__paragraph">
                Wybrałam ten zawód, aby uświadamiać przedsiębiorców zarówno w zakresie ich praw, jak i
                obowiązków. Moja misja to ułatwianie życia – tłumaczenie zawiłości prawnych i przekładanie ich na
                prostszy, bardziej zrozumiały język.
            </p>
            <p class="about__paragraph">
                Moje doświadczenie bazuje na ciągłej edukacji, podnoszeniu jakości świadczonych przeze mnie usług
                oraz na podejmowaniu zawodowych wyzwań.
            </p>
            <p class="about__paragraph">
                Jestem absolwentką prawa na Uniwersytecie Wrocławskim. Ukończyłam również studia
                podyplomowe z zakresu zamówień publicznych na Uniwersytecie Ekonomicznym w Katowicach.
                Dodatkowo zrobiłam też licencjat z socjologii.
            </p>
            <p class="about__paragraph">
                Od 8 lat głównie zajmuję się sprawami związanymi z ochroną danych osobowych oraz doradztwem
                prawnym udzielanym przedsiębiorcom. Moja praca przede wszystkim polega na analizie umów,
                udzielaniu porad prawnych i przeprowadzaniu szkoleń. Jeżeli pojawiam się na sali sądowej, to w
                sprawach dotyczących spadków, działów spadku, zniesienia współwłasności, zasiedzenia i innych
                związanych z nieruchomościami lub postępowaniami przeprowadzanymi przez przedsiębiorców,
                którym doradzam.
            </p>
            <p class="about__paragraph">
                Lubię działać, dlatego jestem wiceprezesem stowarzyszenia Tyskie Szpilki w Biznesie, które wspiera
                kobiety m.in. w prowadzeniu własnego biznesu.
            </p>
            <p class="about__paragraph">
                Prywatnie jestem mamą, książko- i filmoholiczką, wielbicielką kawy i minimalizmu.
            </p>
        </div>

        <div class="about__btn__container">
            <button class="about__btn">Rozwiń więcej</button>
        </div>
    </div>
</section>
