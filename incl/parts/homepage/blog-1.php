<!-- <section class="blog">
  <div class="container">
    <h2 class="blog__header">Blog</h2>
    <p class="blog__paragraph">Porta urna stibulum commodo volutpat a convallis ac, laoreet enim.</p>
    <div class="blog__container owl-carousel">
      <div class="blog__item">
        <div class="blog__image--1 blog__image"></div>
        <p class="blog__text">Jak reagować na żądania osób fizycznych związane z RODO?</p><a class="blog__link" href="#"><span class="fas fa-angle-right"></span></a>
      </div>
      <div class="blog__item">
        <div class="blog__image--2 blog__image"></div>
        <p class="blog__text">DPIA - dokonywanie oceny skutków dla ochrony danych</p><a class="blog__link" href="#"><span class="fas fa-angle-right"></span></a>
      </div>
      <div class="blog__item">
        <div class="blog__image--3 blog__image"></div>
        <p class="blog__text">Jak reagować na żądania osób fizycznych związane z RODO?</p><a class="blog__link" href="#"><span class="fas fa-angle-right"></span></a>
      </div>
      <div class="blog__item">
        <div class="blog__image--4 blog__image"></div>
        <p class="blog__text">DPIA - dokonywanie oceny skutków dla ochrony danych</p><a class="blog__link" href="#"><span class="fas fa-angle-right"></span></a>
      </div>
    </div>
  </div>
</section> -->

<!-- OTHER POSTS -->

<section class="blog s-others s-others--home">
  <div class="container">
    <h2 class="o-heading">Blog</h2>
    <p class="blog__paragraph">Popularne tematy z zakresu prawa w prostej, przystępnej formie</p>
    <?php get_template_part('incl/parts/other-posts'); ?>
  </div>
</section>
