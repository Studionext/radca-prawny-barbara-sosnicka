<header class="hero__contact s-hero s-hero-standard">
  <div class="s-hero__frame">
    <div class="container">
      <div class="navigation">
      <?php
        $args = array(
            'theme_location' => 'main_menu',
            'container' => false,
            'link_before' => '<span>',
            'link_after' => '<span>',
            'items_wrap' => '<ul class="menu">%3$s</ul>',
            'depth' => 0
        );
        echo wp_nav_menu($args);
        ?>
      </div>
      <h1 class="hero__header__contact"><?php the_title(); ?></h1>
    </div>
    <a class="hero__mail" href="mailto:kontakt@kancelariasosnicka.pl">Kontakt@kancelariasosnicka.pl</a>
  </div>
  <div class="s-hero__back-photo"></div>
</header>
