<?php

$orderby = '';

if(is_front_page()):
    $orderby = 'DESC';
else:
    $orderby= 'rand';
endif;
?>

<div class="s-others__container">
  <?php
    $current_ID = get_the_ID();
    $query_others = new WP_Query(array(
      'post_type' => 'post',
      'posts_per_page' => 4,
      'orderby' => $orderby,
      'post__not_in' => array($current_ID)
    ));
    if ($query_others->have_posts()): while ($query_others->have_posts()): $query_others->the_post();
    // var_dump(get_the_post_thumbnail());
  ?>
  <div class="s-others__tile">
    <a class="c-tile" href="<?php the_permalink(); ?>">
      <div
        class="c-tile__photo b-lazy"
        data-src="<?= wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium_large')[0]; ?>">
        <span class="o-caret o-caret--right b-lazy" data-src="<?= TEMPLATE_URL; ?>assets/img/icons/caret-right.png">
        </span>
      </div>
      <h3 class="c-tile__heading"><?php the_title(); ?></h3>
    </a>
  </div>
  <?php endwhile; endif; ?>
</div>
