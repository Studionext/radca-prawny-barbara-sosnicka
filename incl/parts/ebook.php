<section class="ebook">
  <div class="container ebook__wrapper">
    <div class="ebook__container"><img class="ebook__img" src="<?= TEMPLATE_URL; ?>assets/img/pics/ebook.png" alt="Ebook"/>
    </div>
    <?= do_shortcode('[ninja_form id=2]'); ?>
  </div>
</section>
