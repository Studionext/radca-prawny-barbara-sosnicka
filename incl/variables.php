<?php
define( "THEME_PATH", get_theme_root() . '/wireframe-wordpress' . '/' );
define( "TEMPLATE_URL", get_stylesheet_directory_uri() . '/' );
define( "BUILD_URL", TEMPLATE_URL . 'wireframe/build/' );
define( "SITE_URL", site_url() );
define( 'ASSETS_VER', '1.0.3' );
define( 'HOME_TITLE', 'Homepage' );
