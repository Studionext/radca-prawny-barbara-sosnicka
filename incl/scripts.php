<?php

function t4d_include_custom_jquery() {
  wp_deregister_script('jquery');
  wp_enqueue_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js#async', array(), null, false);
}
add_action('wp_enqueue_scripts', 't4d_include_custom_jquery', 0);

function T4_loadScripts(){
    wp_enqueue_script('site_script', TEMPLATE_URL.'assets/js/dist.js', array('jquery'), ASSETS_VER, true);
}
add_action('wp_enqueue_scripts', 'T4_loadScripts');

function T4_loadStyles()
{
    wp_enqueue_style('site_style', TEMPLATE_URL . 'assets/css/style.css', array(), ASSETS_VER);
}
add_action('wp_enqueue_scripts', 'T4_loadStyles');

 ?>
