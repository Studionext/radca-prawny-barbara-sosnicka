<?php
show_admin_bar( false );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'post-formats', array( 'gallery', 'video' ) );
add_post_type_support('attachment', 'custom-fields');

/**
 * Add HTML5 theme support.
 */
function wpdocs_after_setup_theme() {
	add_theme_support( 'html5', array( 'search-form' ) );
}

add_action( 'after_setup_theme', 'wpdocs_after_setup_theme' );

register_nav_menus( array(
	'footer_info_menu' => 'Footer information',
	'footer_menu'      => 'Footer menu',
	'main_menu'        => 'Main menu'
) );


/**
 * Setup homepage
 */
$homepage_by_title = get_page_by_title( HOME_TITLE );

if ( ! $homepage_by_title ) {
	// Create post object
	$my_post = array(
		'post_title'     => wp_strip_all_tags( HOME_TITLE ),
		'post_content'   => '',
		'post_status'    => 'publish',
		'post_type'      => 'page',
		'comment_status' => 'closed',
		'ping_status'    => 'closed',
		'post_author'    => 1,
		'post_category'  => array(),
		'page_template'  => 'templates/page-home.php'
	);

	// Insert the post into the database
	$homepage_created = wp_insert_post( $my_post );

	// Set option for front page
	update_option( 'page_on_front', $homepage_created );
	update_option( 'show_on_front', 'page' );
}

/**
 * Create main menu
 */
if ( ! has_nav_menu( 'main_menu' ) ) {
	// Check if the menu exists
	$menu_name   = 'Main menu';
	$menu_exists = wp_get_nav_menu_object( $menu_name );

	// If it doesn't exist, let's create it.
	if ( ! $menu_exists ) {
		$menu_id = wp_create_nav_menu( $menu_name );

		// Set up default menu items
		wp_update_nav_menu_item( $menu_id, 0, array(
			'menu-item-title'   => __( 'Homepage' ),
			'menu-item-classes' => 'home',
			'menu-item-url'     => home_url( '/' ),
			'menu-item-status'  => 'publish'
		) );

		wp_update_nav_menu_item( $menu_id, 0, array(
			'menu-item-title'  => __( 'Sample page' ),
			'menu-item-url'    => home_url( '/sample-page/' ),
			'menu-item-status' => 'publish'
		) );

		// Then you set the wanted theme location
		$locations              = get_theme_mod( 'nav_menu_locations' );
		$locations['main_menu'] = $menu_id;
		set_theme_mod( 'nav_menu_locations', $locations );

	}
}