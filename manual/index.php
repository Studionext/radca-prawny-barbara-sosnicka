<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Manual</title>
	<style type="text/css">
		.sections {
			text-align: center;
			font-family: Arial, sans-serif;
		}
		.container {
			width: 960px;
			margin: auto
		}
		h1 {
			padding-top: 45px;
			margin-top: 45px;
		}
		h1#navigation {
			margin-top: 0;
			padding-top: 15px
		}
		img, hr {
			max-width: 82%;
			margin: auto;
		}
		img {
			border: solid 1px grey;
            margin-bottom: 20px;
		}
		hr {
			margin-top: 15px;
		}
		p {
			max-width: 75%;
			display: block;
			margin: auto;
			margin-bottom: 15px
		}
	</style>

</head>
<body>

<?php

	$tutorial = ('tutorial.txt');
	$filelines = file($tutorial, FILE_IGNORE_NEW_LINES);
	$exclude = array('.','..','.DS_Store');
	$maindir    = 'assets/';
	$scan = scandir($maindir);
	$total = array();

	foreach ($filelines as $l) {
		if(trim($l)!=""){
			$tutlines[]=$l;
		}
	}

	foreach ($scan as $key => $value) {
		if(!in_array($value, $exclude)){
			$subdir = scandir($maindir.$value);
			$subtotal = $subdir;
			foreach ($subtotal as $k => $v) {
				if(!in_array($v, $exclude)){
					$total[$value][] = $v;
				}
			}

		}
	}

?>


<div class="container">
	<h1 id="navigation">Navigation</h1>
	<ul id="mainnav">
		<?php foreach ($total as $key => $value) {?>

			<li>
				<a href="#<?php echo $key; ?>">
					<?php echo $key; ?>
				</a>
			</li>

		<?php } ?>
	</ul>
</div>
<hr>
<div class="sections">
	<?php $line = 0; ?>
	<?php foreach ($total as $key => $value) {?>

		<?php $src = $maindir.$key; ?>
		<h1 id="<?php echo $key; ?>">
			<?php echo $key; ?>
			<small><a href="#mainnav">Do góry</a></small>
		</h1>

		<?php foreach ($value as $set => $img) {?>
			<p><?php echo $tutlines[$line];?></p>
			<?php $url = $src.'/'.$img; ?>
			<a href="<?php echo $url; ?>" target="_blank">
				<img src="<?php echo $url; ?>"/>
			</a>
			<?php $line++; ?>
		<?php }?>
		<hr>
	<?php } ?>
</div>

</body>
</html>
